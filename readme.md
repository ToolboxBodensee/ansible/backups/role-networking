Debian Networking configuration
===============================

Ansible role to configure network interfaces on Debian.


Variables
---------


Files
-----

* `interfaces.conf`:
  The main Debian networking configuration file.

* `interfaces.d`:
  Snippets of Debian networking configuration.
